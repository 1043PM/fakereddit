import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/MainPage.vue'
Vue.use(Router)

export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'home',      
      component: Home,
      children: [
        {
          path: '/home',
          name: 'home',
          component: () => import( './components/MainPage.vue' )
        },
      ]
    }
  ]
})
