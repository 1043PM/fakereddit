import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import store from './store'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  Buefy,
  render: h => h(App),
}).$mount('#app')
